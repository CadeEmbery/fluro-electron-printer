

Git clone this repository

Then run 'npm install'
and run 'bower install'

Then run 'grunt'

Grunt will just run a watcher to see when any files change, all of the files are under the 'lib' folder, 
if you update any file in the lib folder it will then then recompile all the files it needs into the 'app' folder

Once you're ready to give it a crack and run the application do this
'npm run dist'

This will then use electron to build and compile the app for you, the files will be created under the 'dist' folder
and then you can open/install and run to see what's happening.

/main.js is the main electron file, you shouldn't need to edit this apart from commenting out line 33 (whether to pop open devTools on startup)
