module.exports = function(grunt) {

    "use strict";

    var rewrite = require('connect-modrewrite');



    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),



        //open: {
        //dev: {
        //  path: 'http://0.0.0.0:9001',
        //app: 'Google Chrome'
        //},
        //},



        connect: {
            main: {
                options: {
                    port: 9001,
                    base: 'app'
                }
            }
        },

        /////////////////////////////////////////

        watch: {
            options: {
                livereload: {
                    port: 9002
                }
            },

            //Concatenate all the sass files, run sass and autoprefixer
            css: {
                files: [
                    'lib/components/**/*.scss',
                    'lib/scss/**/*.scss',
                ],

                tasks: ['concat:sass', 'sass', 'autoprefixer']
            },

            //Concatenate all the html templates and combine them into
            //a single file (js/templates.js)
            html: {
                files: [
                    'lib/components/**/*.html',
                ],
                // tasks: ['ngtemplates']
                tasks: ['copy:html']
            },

            //Concatenate all the javascript into a single file
            js: {
                files: [
                    'lib/components/**/*.js',
                ],
                tasks: ['concat']
            },
            //Watch for changes to the bower file and wire
            //the dependencies into index.html
            bower: {
                files: [
                    'bower.json',
                ],
                tasks: ['wiredep']
            }
        },

        /////////////////////////////////////////////////

        autoprefixer: {
            single_file: {
                src: 'app/css/style.css',
                dest: 'app/css/style.css'
            }
        },

        /////////////////////////////////////////////////

        //Compile the SASS
        sass: {
            build: {
                files: {
                    //'app/css/style.css': 'lib/components/**/*.scss'
                    'app/css/style.css': 'lib/scss/.tmp.style.scss'
                }
            }
        },

        /////////////////////////////////////////////////

        //Minify the CSS
        cssmin: {
            build: {
                files: {
                    'compiled/css/style.min.css': ['app/css/style.css']
                }
            }
        },

        /////////////////////////////////////////////////

        // //Combine all templates into a single file
        // ngtemplates: {
        //     fluro: {
        //         cwd: './lib/components',
        //         src: '**/*.html',
        //         dest: 'app/js/templates.js',
        //         options: {
        //             //usemin:'/js/templates.min.js',
        //             htmlmin: {
        //                 collapseBooleanAttributes: true,
        //                 collapseWhitespace: true,
        //                 removeAttributeQuotes: true,
        //                 removeComments: true, // Only if you don't use comment directives! 
        //                 removeEmptyAttributes: true,
        //                 removeRedundantAttributes: true,
        //                 removeScriptTypeAttributes: true,
        //                 removeStyleLinkTypeAttributes: true
        //             }
        //         }
        //     }
        // },

        /////////////////////////////////////////////////

        copy: {
            html: {
                files: [
                // {
                //     // expand: true,
                //     cwd: 'lib/components',
                //     src: [
                //     '/**/*.html',
                //     ],
                //     dest: 'app'
                // }, 
                {
                    expand: true,
                    cwd: 'lib/components',
                    src: [
                        '**/*.html',
                    ],
                    dest: 'app'
                }],
            },
        },

        /////////////////////////////////////////////////

        //Wire the bower dependencies into the index html file
        wiredep: {
            task: {
                src: [
                    'app/index.html', // .html support...
                ],
                options: {
                    // cwd: '',
                    exclude: [
                        '/bower_components/bootstrap/dist/js/bootstrap.js'
                    ]
                },
                fileTypes: {
                    html: {
                        block: /(([\s\t]*)<!--\s*bower:*(\S*)\s*-->)(\n|\r|.)*?(<!--\s*endbower\s*-->)/gi,
                        detect: {
                            js: /<script.*src=['"](.+)['"]>/gi,
                            css: /<link.*href=['"](.+)['"]/gi
                        },
                        replace: {
                            js: '<script src="{{filePath}}"></script>',
                            css: '<link rel="stylesheet" href="{{filePath}}" />'
                        }
                    }
                }

            }
        },

        /////////////////////////////////////////////////

        //Concatenate all the build js files
        concat: {
            js: {
                src: ['lib/components/**/*.js'],
                dest: 'app/js/app.js'
            },

            sass: {
                src: [
                    'lib/scss/style.scss',
                    'lib/components/**/*.scss'
                ],
                dest: 'lib/scss/.tmp.style.scss'
            },

            //Combine the vendor scripts
            //the app scripts
            //and the templates into a single file
            combine: {
                src: [
                    'app/js/vendor.js',
                    'app/js/app.js',
                    'app/js/templates.js'
                ],
                dest: 'app/js/app.combined.js'
            }
        },


        /////////////////////////////////////////////////////////////

        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            app: {
                files: {
                    'app/js/app.annotated.js': ['app/js/app.combined.js']
                }
            }
        },

        /////////////////////////////////////////////////////////////

        uglify: {
            options: {
                mangle: false,
                // compress: false,
                beautify: true
            },
            dist: {
                options: {
                    // mangle: {
                    //     except: ['angular']
                    // },
                    mangle: false,
                    // compress: true,
                    beautify: true,
                },
                src: ['app/js/app.annotated.js'],
                dest: 'app/js/app.min.js'
            }
        }


        // nwjs: {
        //     options: {
        //         // 'win32','win64',
        //         platforms: ['osx64'],
        //         buildDir: './built', // Where the build version of my NW.js app is saved
        //         macIcns: './icons/icon.png.icns',
        //         winIco: './icons/icon.ico'
        //     },
        //     src: ['./app/**/*'] // Your NW.js app
        // }


    });


    //Default Watching Task
    grunt.registerTask('default', ['connect', 'watch']);

    //Build Script
    grunt.registerTask('build', [
        //Compile the SASS
        'concat:sass', 
        'sass', 
        'autoprefixer',
        'concat:js', 
        'ngtemplates',
        'concat:combine',
        'ngAnnotate',
        'uglify:dist',
        'fluro'
    ]);

    grunt.registerTask('fluro', 'add fluro meta tags', function() {


        var liveAPIURL = 'https://api.fluro.io';

        var config = '<meta property="fluro_url" content="' + liveAPIURL + '">';
        var beforecss = '';
        var aftercss = '';
        var googleanalytics = '';
        var appdata = '';
        var socketserver = '<script src="' + liveAPIURL + '/socket.io/socket.io.js"></script>';


        var index = grunt.file.read('app/index.html');
        console.log('Adding Fluro Injections');
        index = index.replace('<!-- fluro:config -->', config);
        index = index.replace('<!-- fluro:beforecss -->', beforecss);
        index = index.replace('<!-- fluro:aftercss -->', aftercss);
        index = index.replace('<!-- fluro:googleanalytics -->', googleanalytics);
        index = index.replace('<!-- fluro:appdata -->', appdata);
        index = index.replace('<!-- fluro:socket -->', socketserver);

        console.log('Removing Fluro local tags')
        index = index.replace(/([\s\S]*?)<!-- fluro:devstart -->[\s\S]*?<!-- fluro:devend -->/g, '$1');


        // console.log(index);
        grunt.file.write('app/index.html', index);
    });


};