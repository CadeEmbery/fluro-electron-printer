app.service('PrintService', function($interval, $timeout, $localStorage) {

    var controller = {
        queue: [],
        history: [],
        availablePrinters: [],
        labelWidth: '90mm',
        labelHeight: '29mm',
    };

    ////////////////////////////////////////

    //Create a browser window renderer
    var renderer = new BrowserWindow({
        width: 800,
        show: false,
        height: 600,
        webSecurity: false,
    });

    ////////////////////////////////////////

    console.log('SUPPORTED FORMATS', printer.getSupportedPrintFormats())

    ////////////////////////////////////////


    //Get the webContents Object
    var webContents = renderer.webContents;
    // webContents.openDevTools()



    // printWindow.once('ready-to-show', () => {
    //     printWindow.show()
    // })

    ////////////////////////////////////////

    var busy;
    var currentStatus;




    ////////////////////////////////////////////////////////////////////////////////

    controller.getPaperSizes = function(trace) {

        if (!controller.printer) {
            //No printer is selected
            return;
        }




        if (platform == 'win32' || platform == 'win64') {
            return;
        }

        //Update driver options
        controller.driverOptions = window.printer.getPrinterDriverOptions(controller.printer.name);

        //Get all the page sizes available
        var pageSizes = _.get(controller, 'driverOptions.PageSize');

        //Get only the page sizes that can be printed right now
        controller.pageSizes = _.reduce(pageSizes, function(results, option, key) {

            //If it's available
            if (option) {
                //Add the option
                results.push(key);
            }

            return results;

        }, []);

        /////////////////////////////

        //If only one page size is available
        if (controller.pageSizes.length == 1) {
            //Select it
            $localStorage.pageSize = controller.pageSizes[0];
        }

        /////////////////////////////

        if (trace) {
            console.log('Driver Options', controller.driverOptions);
        }
    }



    ////////////////////////////////////////////////////////////////////////////////

    controller.deselectPrinter = function(printer) {
        delete controller.printer;
        delete controller.pageSizes;
        delete controller.driverOptions;
        delete $localStorage.pageSize;
        delete $localStorage.deviceName;
    }

    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    controller.getPrinterStatus = function(printer) {

        //If no printer
        if (!printer) {
            return 'danger';
        }

        //Get the status
        var status = _.get(printer, "options['printer-state-reasons']") || printer.status;

        switch (status) {
            case 'none':
            case 'IDLE':
                return 'success';
                break;
            default:
                return 'warning';
                break;
        }
    }


    ////////////////////////////////////////////////////////////////////////////////

    controller.print = function() {

        if (busy) {
            //Do nothing
            return console.log('printer is busy');
        }

        if (!renderer) {
            return console.log('No renderer specified for print service');
        }

        ////////////////////////////////////////////

        //Nothing in the queue
        if (!controller.queue.length) {
            return console.log('Nothing in queue');
        }

        ////////////////////////////////////////////

        if (!controller.availablePrinters || !controller.availablePrinters.length) {
            return console.log('No available printers connected');
        }

        ////////////////////////////////////////////

        if (!$localStorage.deviceName || !$localStorage.deviceName.length) {
            return console.log('No printer device name has been specified');
        }

        ////////////////////////////////////////////

        //Find a printer with the same device name
        var selectedPrinter = _.find(controller.availablePrinters, {
            name: $localStorage.deviceName
        });

        if (!selectedPrinter) {
            return console.log('No printer matches deviceName', $localStorage.deviceName);
        }

        ////////////////////////////////////////////

        //If no paper size has been selected
        if (!$localStorage.pageSize) {
            if (!controller.pageSizes || !controller.pageSizes.length) {
                controller.getPaperSizes();
            }
        }

        ////////////////////////////////////////////

        //Get the status of the printer
        var printerStatus = selectedPrinter.options['printer-state-reasons'];

        ////////////////////////////////////////////

        switch (printerStatus) {
            case 'none':
            case 'IDLE':
                break;
            default:
                console.log('Might not be able to print because status is:', printerStatus);
                break;
        }
        // if(printerStatus && printerStatus != 'none') {
        //     return console.log('Printer Status', printerStatus);
        // }

        /////////////////////////////////////////

        //We are busy printing now
        busy = true;


        //Get everything in the queue at this point
        var queueLength = controller.queue.length;
        var jobs = controller.queue.splice(0, queueLength);

        //Add all of the nametags to the same html string
        var printHTML = _.map(jobs, function(job) {
            return job.html;
        }).join('');


        if (!printHTML || !printHTML.length) {
            console.log('No html was provided to printer');
            return busy = false;
        }

        /////////////////////////////////////////
        /////////////////////////////////////////
        /////////////////////////////////////////

        // Get all the CSS from the original
        var cssFiles = _.map($('link'), function(link) {
            return '<link href="' + $(link).attr('href') + '" rel="stylesheet" type="text/css">';
        }).join('');

        /////////////////////////////////////////
        /////////////////////////////////////////
        /////////////////////////////////////////

        var extraTags = [];

        // extraTags.push('<style type="text/css">html, body {margin:0; padding:0; width:100%; height: 100%; overflow:hidden;}  </style>');
        // extraTags.push('<style>@page { size: '+width + ' ' + height + ';margin: 0;// 3mm;}}</style>');


        var width = $localStorage.labelWidth || controller.labelWidth;
        var height = $localStorage.labelHeight || controller.labelHeight;

        extraTags.push('<style type="text/css">@page { size: ' + width + ' ' + height + '; margin:0; }</style>');

        /////////////////////////////////////////
        /////////////////////////////////////////

        var headerHTML = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width,maximum-scale=1.0"></head><body>';
        var footerHTML = '</body></html>';

        /////////////////////////////////////////

        //Load the RAW HTML Into the window
        var renderHTML = headerHTML + cssFiles + printHTML + extraTags.join('') + footerHTML;

        //Add in the data boom
        // renderer.loadURL("data:text/html," + renderHTML);

        renderer.loadURL('data:text/html;charset=UTF-8,' + encodeURIComponent(renderHTML), {
            baseURLForDataURL: `file://${__dirname}/`
        })


        /////////////////////////////////////////
        /////////////////////////////////////////

        console.log('LOADING PREVIEW HTML', renderHTML)
        
        /////////////////////////////////////////
        /////////////////////////////////////////

        webContents.on("did-finish-load", function() {
            console.log('HTML has Loaded');
            $timeout(renderShouldBeLoaded, 200);
        });




        /////////////////////////////////////////
        /////////////////////////////////////////

        //Wait for a second so its rendered
        

        function renderShouldBeLoaded() {

            console.log('buffer time complete')

            var printConfig = {
                name: controller.printer.name,
                deviceName: controller.printer.name,
                printer: controller.printer.name,
                landscape: true,
                headerFooterEnabled: false,
                // autoprint: true,
                // printBackground: true,
                // pageSize:'90mm 29mm',
                // mediaSize: {
                //     name: 'Fluro Label',
                //     width_microns: 90300,
                //     height_microns: 2900,
                //     custom_display_name: 'Fluro Label',
                //     is_default: true
                // },
                // marginsCustom: {
                //     "marginBottom": 0,
                //     "marginLeft": 0,
                //     "marginRight": 0,
                //     "marginTop": 0
                // },
                marginsType: 1,
                // silent:true,
            }


            ///////////////////////////////////////////////


            if (controller.previewBeforePrinting) {
                console.log('PRINT WITH PREVIEW')
                    // printConfig.silent = false;
                webContents.print(printConfig, function(res) {
                    console.log('PREVIEW PRINT', res);


                });

                return completeJob();

            }



            ///////////////////////////////////////////////



            console.log('PDF Prerender started', printConfig);
            console.log('WEBCONTENTS', webContents);

            webContents.printToPDF(printConfig, PDFComplete);



            ///////////////////////////////////////////////

            function PDFComplete(err, buffer) {
                if (err) {
                    console.log('PDF RENDER ERROR', err);
                    return completeJob();
                }

                
                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////

                var fs = require('fs-extra');

                var filepath = electron.remote.app.getPath('appData') + '/fluro/fluro-printer-label.pdf';
                console.log('PDF Example', filepath);

                fs.ensureFile(filepath, function(err) {

                	if (err) {
                        return console.log('Failed to ensure pdf', err);
                    }


                	fs.writeFile(filepath, buffer, function(err) {
                     if (err) {
                         return console.log('Failed to write pdf', err);
                     }

                    console.log("Write PDF successfully.");
                	});
                })

                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////

                console.log('PDF Prerender complete', buffer.length);

                if (buffer.length < 700) {
                    //Try it a second time
                    console.log('RENDERING ISSUE - NO DATA IN BUFFER');
                    return completeJob();
                }


                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////

                var options = {
                    // media: 'Letter',
                    'fit-to-page': true
                }

                //////////////////////////////////////////

                if ($localStorage.pageSize) {
                    options.media = $localStorage.pageSize;
                    console.log('Using selected pageSize', $localStorage.pageSize, 'Of', controller.pageSizes);
                } else {
                    if (controller.pageSizes && controller.pageSizes.length) {
                        $localStorage.pageSize = options.media = controller.pageSizes[0];
                        console.log('Using first page size available');
                    }
                }

                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////

                var printDirectType = 'PDF';

                if (platform == 'win32' || platform == 'win64') {
                    printDirectType = 'XPS2GDI';
                }


                console.log('PRINT DIRECT TYPE', printDirectType);

                //////////////////////////////////////////
                //////////////////////////////////////////
                //////////////////////////////////////////

                controller.printing = true;

                //////////////////////////////////////////
                //////////////////////////////////////////

                var printDirectSettings = {
                    printer: controller.printer.name,
                    data: buffer,
                    type: printDirectType,
                    options: options,
                    // options:printConfig,
                    success: function(jobID) {
                        controller.printing = false;
                        console.log('Queued in printer', jobID);
                    },
                    error: function(err) {
                        controller.printing = false;
                        console.log('Print Queue Error', err);
                    },
                }

                //////////////////////////////////////////

                console.log('Print Direct', printDirectSettings)

                //////////////////////////////////////////

                printer.printDirect(printDirectSettings);

            }

            ////////////////////////////////////////////

            completeJob();

            ////////////////////////////////////////////

            function completeJob() {

                //Find any name tags that should be able to reprinted
                var historicalJobs = _.chain(jobs)
                    .filter(function(job) {
                        return !job.options.once;
                    })
                    .value();

                ////////////////////////////////////////////

                //Add them into history
                controller.history = controller.history.concat(historicalJobs);

                ////////////////////////////////////////////

                //Set the printer to be available again for more tickets
                busy = false;

                ////////////////////////////////////////////

                //Check if theres more to print
                return nextInQueue();
            }

            // });
        }
        // });
    }

    ////////////////////////////////////////////////////////////////////////////////

    /**
        function checkPrinterQueue() {
            
            nw.Window.open('print.html', {
                // show: false,
                // width: 100,
                // height:600,
            }, function(newWindow) {

                newWindow.on('loaded', function() {

                    var printDocument = newWindow.window.document;
                    printDocument.write(html);


                    setTimeout(function() {

                    // return;
                    // const filename = 'result.pdf';

                    console.log('Printing to ', $scope.settings.printer);
                    newWindow.print({
                        printer: $scope.settings.printer,
                        landscape: true,
                        headerFooterEnabled: false,
                        // autoprint:false,
                        // mediaSize: {
                        //     name: 'Label',
                        //     width_microns: 90300,
                        //     height_microns: 2900,
                        //     custom_display_name: 'Label',
                        //     is_default: true
                        // },

                        marginsCustom: {
                            "marginBottom": 0,
                            "marginLeft": 0,
                            "marginRight": 0,
                            "marginTop": 0
                        },
                        marginsType: 3,

                    });

                    newWindow.close();
                    // nw.Shell.openItem(filename);

                    },500)
                });
                



            });
        }

        /**/
    ////////////////////////////////////////////////////////////////////////////////

    controller.selectPrinter = function(printer) {


        //Select the printer
        controller.printer = printer;
        $localStorage.deviceName = printer.name;


        if ($localStorage.deviceName != printer.name) {
            console.log('Change Printer');
            //Get the paper sizes
            controller.getPaperSizes(true);
        } else {
            //Get the paper sizes
            controller.getPaperSizes();
        }



        //Get the status of the newly selected printer
        var newStatus = printer.options['printer-state-reasons'];

        if (currentStatus != newStatus) {
            // console.log('Printer status changed')
            currentStatus = newStatus;
            return controller.print();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////

    controller.isSelected = function(printer) {
        $localStorage.deviceName == printer.name
    }

    ////////////////////////////////////////////////////////////////////////////////

    function guidGenerator() {
        var S4 = function() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }

    ////////////////////////////////////////////////////////////////////////////////

    controller.reprint = function(job) {
        controller.add(job.title, job.html)
    }

    ////////////////////////////////////////////////////////////////////////////////


    controller.add = function(title, html, options) {

        if (!options) {
            options = {}
        }

        ////////////////////////////////////////////////////

        // console.log('ADD JOB', title, html, withPreview || 'Silent');
        var job = {
            title: title,
            html: html,
            id: guidGenerator(),
            created: new Date(),
            options: options,
        }

        controller.queue.push(job);

        $timeout(function() {
            return nextInQueue();
        }, 500);
    }

    ////////////////////////////////////////////////////////////////////////////////

    controller.remove = function(job) {
        _.pull(job);

        $timeout(function() {
            return nextInQueue();
        }, 500);
    }

    ////////////////////////////////////////////////////////////////////////////////

    function nextInQueue() {
        //Keep on keeping on
        return controller.print();

    }


    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    //Get the printers
    controller.refreshPrinterList = function() {

        //Update the printer list
        var printers = webContents.getPrinters();
        controller.availablePrinters = printers;

        ////////////////////////////////////////

        //If a deviceName has already been specified
        if ($localStorage.deviceName && $localStorage.deviceName.length) {

            // var currentPrinterName = _.get(controller, 'printer.name');
            // if (currentPrinterName == $localStorage.deviceName) {
            //     return;
            // }

            //////////////////////////////////////

            //Find a matching printer in the list
            var matchingPrinter = _.find(printers, {
                name: $localStorage.deviceName
            });

            //If a match was found
            if (matchingPrinter) {
                //Select the printer
                return controller.selectPrinter(matchingPrinter);
            }
        }

        ////////////////////////////////////////

        //If there is only one printer available
        if (printers.length == 1) {
            //Select the only printer available
            return controller.selectPrinter(printers[0]);
        }
    }


    ////////////////////////////////////////////////////////////////////////////////

    //Refresh the list straight away
    controller.refreshPrinterList();

    //Refresh the printer list ever 5 seconds
    $interval(controller.refreshPrinterList, 2000);

    ////////////////////////////////////////////////////////////////////////////////

    return controller;

});