app.filter('reverse', function() {
  return function(items) {
  	if(!items) {
  		return items;
  	}
  	
  	var sliced = items.slice();


    return sliced.reverse();
  };
});