app.controller('HomeController', function($scope, $q, $templateCache, $compile, $interpolate, $timeout, $localStorage, $http, $interval, FluroSocket, PrintService) {


    $scope.openDevTools = function() {

        var current = electron.remote.getCurrentWebContents();

        console.log('Open Dev Tools');
        current.openDevTools({mode: 'undocked'})
    }


    ////////////////////////////////////////

    $scope.localStorage = $localStorage;

    if (!$localStorage.printerID) {
        $localStorage.printerID = 'Printer1'
    }

    ////////////////////////////////////////


    $scope.settings = {
        uiState: 'queue',
    };

    ////////////////////////////////////////

    $scope.printService = PrintService;

    ////////////////////////////////////////
    ////////////////////////////////////////
    ////////////////////////////////////////

    function createNameTag(checkin) {

        var deferred = $q.defer();

        ///////////////////////////////////////

        //Create a new scope
        var $newScope = $scope.$new(true);
        $newScope.checkin = checkin;

        ///////////////////////////////////////

        //Create the new directive
        var newDirective = angular.element('<div><nametag ng-model="checkin"></nametag></div>');
        // element.append(newDirective);
        $compile(newDirective)($newScope);


        $timeout(function() {
            // var nametagTemplate = $templateCache.get('nametag/nametag.html');
            // var compiledNametag = $compile('<div><nametag ng-model="checkin"></nametag></div>')(newScope)

            var html = newDirective[0].innerHTML;

            deferred.resolve(html);
        }, 100);

        return deferred.promise;
    }

    ////////////////////////////////////////

    function createTicket(checkin) {

        var deferred = $q.defer();

        ///////////////////////////////////////

        //Create a new scope
        var $newScope = $scope.$new(true);
        $newScope.checkin = checkin;

        ///////////////////////////////////////

        //Create the new directive
        var newDirective = angular.element('<div><ticket ng-model="checkin"></ticket></div>');
        // element.append(newDirective);
        $compile(newDirective)($newScope);


        $timeout(function() {
            // var nametagTemplate = $templateCache.get('nametag/nametag.html');
            // var compiledNametag = $compile('<div><nametag ng-model="checkin"></nametag></div>')(newScope)

            var html = newDirective[0].innerHTML;

            deferred.resolve(html);
        }, 100);

        return deferred.promise;
    }

    ////////////////////////////////////////

    $scope.addToPrintQueue = function(data, isReprint) {

        //Get the rendered name tag
        var nametag = createNameTag(data);
        var ticket;


        /////////////////////////////////////

        if ($localStorage.printTickets && !isReprint) {
            console.log('Create parent ticket also')
            ticket = createTicket(data);
        }

        /////////////////////////////////////

        //Wait for both the nametag and the ticket to be rendered
        var promises = _.compact([nametag, ticket]);

        console.log('Job promises', promises);

        /////////////////////////////////////

        $q.all(promises).then(function(results) {

            nametagHTML = results[0];
            ticketHTML = results[1];

            var name = data.firstName + ' ' + data.lastName;

            console.log('Add nametag to queue');
            PrintService.add(name, nametagHTML);

            if(ticketHTML) {
                console.log('Add Ticket to queue')
                PrintService.add('PARENT ' + name, ticketHTML, {once:true});
            }

            // /////////////////////////////////////

            // //Once the nametag has been rendered
            // nametag.then(function(nametagHTML) {

            //     //Get the name of the checkin
            //     var name = data.firstName + ' ' + data.lastName;

            //     //Add the name to the queue
            //     PrintService.add(name, nametagHTML);
            // });
        });


       

        

        // if(ticket) {
        //     //Once the ticket has been rendered
        //     ticket.then(function(ticketHTML) {

        //         //Get the name of the checkin
        //         var name = 'TICKET ' + data.firstName + ' ' + data.lastName;

        //         //Add the name to the queue
        //         PrintService.add(name, ticketHTML, {once:true});
        //     });
        // }
    }

    //////////////////////////////////////////////////////////

    $scope.testCheckin = {
        firstName: 'James',
        lastName: 'Bond',
        hashCode: 'ABC-123',
        groupNames:['Group One', 'Group Two'],
        event: {
            title: 'Kids Event - Sunday Morning Service'
        },
    }

    //////////////////////////////////////////////////////////////////

    function newCheckin(event, isReprint) {
        console.log('checkin event', event)

        var checkinPrinterID = String(event.data.printerID).toLowerCase();
        var stationPrinterID = String($localStorage.printerID).toLowerCase();

        if (checkinPrinterID == stationPrinterID && checkinPrinterID) {
            //This printer should print the label

            $scope.addToPrintQueue(event.data, isReprint);
        } else {
            //someone elses problem
        }


        // $timeout(function() {
        //     console.log('PUSH Checkin event!', checkin);
        //     $scope.queue.push(checkin);


        // })
    }

    ////////////////////////////////////////

    FluroSocket.on('checkin', function(event) {
        console.log('CHECKIN SOCKET EVENT')
        newCheckin(event);
    });
    FluroSocket.on('checkin.reprint', function(event) {
        console.log('REPRINT SOCKET EVENT')
        newCheckin(event, true);
    });



});