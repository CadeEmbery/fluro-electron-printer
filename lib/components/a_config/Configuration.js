var app = angular.module('fluro', [
    'ngAnimate',
    'ngResource',
    'ui.router',
    'ngTouch',
    'fluro.config',
    'fluro.access',
    'fluro.validate',
    'fluro.interactions',
    'fluro.content',
    'fluro.asset',
    'fluro.socket',
    'fluro.video',
    'angular.filter',
    'formly',
    'formlyBootstrap',
    'ui.bootstrap'
])

/////////////////////////////////////////////////////////////////////

var electron;
var printer;
var BrowserWindow 
var ipcRenderer;
var os;
var platform;

if(window.require) {
    os = require('os');
    // printer = require('electron-printer');
        electron = require('electron');

    printer = electron.remote.require('printer')

    BrowserWindow = electron.remote.BrowserWindow;
    ipcRenderer = electron.remote.ipcRenderer;

    console.log('OS', os)

    platform = os.platform();
    // console.log('ELECTRON', electron);
    // console.log('APP', app)
    console.log('PRINTER', printer);
    console.log('ELECTRON', electron);
    console.log('ELECTRON REMOTE', electron.remote);

    



    // const electronPrinter = require('electron-print');
     
    //  console.log('TEST ELECTRON PRINTER', electronPrinter);
    // // app.on('ready', function() {
    //     electronPrinter.print("Text sent to printer.")
    // // });
     

    // console.log('PRINTER LIST', printer.getPrinters);
} else {
    console.log('NOT RUNNING IN ELECTRON BUT IN BROWSER')
}


/**
var win;

if(window.nw) {


    win  = nw.Window.get();
    win.showDevTools();

} else {
    console.log('NW is not defined');
    win = {
        getPrinters:function() {
            return [];
        }
    }
}
/**/
/////////////////////////////////////////////////////////////////////

function getMetaKey(stringKey) {
    var metas = document.getElementsByTagName('meta');

    for (i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("property") == stringKey) {
            return metas[i].getAttribute("content");
        }
    }
    return "";
}


console.log('init printer application');

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

app.config(function($stateProvider, $compileProvider, $httpProvider, FluroProvider, $urlRouterProvider, $locationProvider) {

   
    $compileProvider.debugInfoEnabled(false);
    ///////////////////////////////////////////////////

    var access_token = getMetaKey('fluro_application_key');

    //API URL
    var api_url = getMetaKey('fluro_url');

    FluroProvider.set({
        apiURL: api_url,
        token: access_token,
        sessionStorage: false,
    });

    ///////////////////////////////////////////

    //Http Intercpetor to check auth failures for xhr requests
    if (!access_token) {
        $httpProvider.defaults.withCredentials = true;
    }

    $httpProvider.interceptors.push('FluroAuthentication');

    ///////////////////////////////////////////

    // $locationProvider.html5Mode(true);

    ///////////////////////////////////////////
    ///////////////////////////////////////////

    $stateProvider.state('home', {
        url: '/',
        templateUrl: 'routes/home/home.html',
        controller: 'HomeController',
        resolve: {
            // session: function($stateParams, FluroContent) {
            //     return FluroContent.endpoint('session').get().$promise;
            // },
        }
    });


    ///////////////////////////////////////////

    // $stateProvider.state('packages', {
    //     url: '/',
    //     templateUrl: 'routes/packages/packages.html',
    //     controller: 'PackagesController',
    //     resolve: {
    //         // session: function($stateParams, FluroContent) {
    //         //     return FluroContent.endpoint('session').get().$promise;
    //         // },
    //     }
    // });

    ///////////////////////////////////////////

    $urlRouterProvider.otherwise("/");


});

/////////////////////////////////////////////////////////////////////

app.run(function($rootScope, $sessionStorage, $localStorage, ConnectionService,FluroTokenService, Asset, $timeout, FluroContent, FluroSocket, FluroBreadcrumbService, FluroScrollService, $location, $timeout, $state) {

    //Global variable for if we are testing live
    //or locally
    $rootScope.staging = true;
    $rootScope.asset = Asset;
    $rootScope.$state = $state;
    $rootScope.session = $sessionStorage;
    $rootScope.breadcrumb = FluroBreadcrumbService;
    $rootScope.connection = ConnectionService;
    ConnectionService.start();

    //////////////////////////////////////////////////////////////////


    $rootScope.signout = function() {
        console.log('Clear local storage and SIGN OUT')
        FluroTokenService.logout();
        $localStorage.$reset();
        $sessionStorage.$reset();
    }

    //////////////////////////////////////////////////////////////////

    console.log('check session');

    //Get the session of the current user
    FluroContent.endpoint('session')
        .get()
        .$promise.then(function(res) {

            console.log('has session', res);
            $rootScope.user = res;
        }, function(err) {
            console.log('no session');
            $rootScope.user = null;
        });


    //////////////////////////////////////////////////////////////////

    $rootScope.logout = function() {
        //Sign out
        $rootScope.user = null;
    }

    //////////////////////////////////////////////////////////////////



    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, error) {
        //Close the sidebar
        $rootScope.sidebarExpanded = false;
    });


    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        throw error;
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, error) {
        $rootScope.currentState = toState.name;
    });

    //////////////////////////////////////////////////////////////////


    $rootScope.getTypeOrDefinition = function(item, defaultIfNoneProvided) {
        if (item.definition && item.definition.length) {
            return item.definition;
        }

        if (!item._type && defaultIfNoneProvided) {
            return defaultIfNoneProvided;
        }


        return item._type;
    }



    //////////////////////////////////////////////////////

    //Make touch devices more responsive
    // FastClick.attach(document.body);

});