app.service('ConnectionService', function(FluroContent, $interval, Fluro, $http, $interval) {

    var controller = {
        online: true,
    }

    ////////////////////////////////////////////////    
    ////////////////////////////////////////////////

    var inflightRequest;

    controller.check = function() {


        if (inflightRequest) {
            return inflightRequest;
        }

        // console.log('check connectivity');
        //Create a new request
        inflightRequest = $http.head(Fluro.apiURL + '/healthcheck');

        inflightRequest.then(function(res) {
            // console.log('online')
            controller.online = true;
            inflightRequest = null;
        }, function(err) {
            controller.online = false;
            // console.log('offline')
            inflightRequest = null;
        });

        return inflightRequest;

    }

    ////////////////////////////////////////////////

    var timer;

    controller.start = function() {
        if (!timer) {
            timer = $interval(controller.check, 5000);
        }
    }

    controller.stop = function() {
        if (timer) {
            $interval.cancel(timer);
        }
    }

    ////////////////////////////////////////////////

    return controller;

});