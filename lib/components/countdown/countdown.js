app.directive('countdown', function() {
    return {
        restrict: 'E',
        replace: true,
        scope:{
            model:'=ngModel',
        },
        templateUrl:'countdown/countdown.html',
        link: function(scope, element, attrs) {
          
        },
        controller:'CountdownController',
    };
});


app.controller('CountdownController', function($scope) {
    console.log('Countdown controller')
});