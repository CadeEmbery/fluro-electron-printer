////////////////////////////////////////////////////////////////////////

app.directive('nametag', [function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'nametag/nametag.html',
        controller: function($scope) {

            /////////////////////////////////////////////////////

            $scope.getDisplayName = function() {
                var names = $scope.model.firstName + ' ' + $scope.model.lastName;

                if(names.length >= 15) {
                    return $scope.model.firstName + ' ' + $scope.model.lastName.substr(0, 1);
                } else {
                    return names;
                }
            }

            /////////////////////////////////////////////////////

            if($scope.model.groupNames && $scope.model.groupNames.length) {
                // console.log('Group names')
                $scope.groupNames = $scope.model.groupNames.join(', ');
            }

            /////////////////////////////////////////////////////
        }
    }

}]);


////////////////////////////////////////////////////////////////////////

app.directive('ticket', [function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'nametag/ticket.html',
        controller: function($scope) {
            /////////////////////////////////////////////////////

            $scope.getDisplayName = function() {
                var names = $scope.model.firstName + ' ' + $scope.model.lastName;

                if(names.length >= 15) {
                    return $scope.model.firstName + ' ' + $scope.model.lastName.substr(0, 1);
                } else {
                    return names;
                }
            }
        }
    }

}]);

