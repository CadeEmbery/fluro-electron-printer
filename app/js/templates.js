angular.module('fluro').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('accordion/accordion.html',
    "<div class=accordion ng-class={expanded:settings.expanded}><div class=accordion-title ng-click=\"settings.expanded = !settings.expanded\"><div class=container-fluid><div class=text-wrap><h3 class=title><i class=\"fa fa-angle-right pull-right\" ng-class=\"{'fa-rotate-90':settings.expanded}\"></i> <span ng-transclude=title></span></h3></div></div></div><div class=accordion-body><div ng-class=\"{'container':wide, 'container-fluid':!wide}\"><div ng-class=\"{'text-wrap':!wide}\" ng-transclude=body></div></div></div></div>"
  );


  $templateCache.put('admin-date-select/admin-date-select.html',
    "<div class=dateselect ng-class={open:settings.open}><div class=btn-group><a class=\"btn btn-default\" ng-class={active:settings.open} ng-click=\"settings.open = !settings.open\"><i class=\"fa fa-calendar\"></i> <span ng-bind-html=\"readable | trusted\"></span></a></div><dpiv class=popup><div class=datetime><div uib-datepicker class=datepicker datepicker-options=datePickerOptions ng-model=settings.dateModel></div></div></dpiv></div>"
  );


  $templateCache.put('countdown/countdown.html',
    "<div class=countdown><div class=row><div class=col-xs-3><div class=countdown-value>1</div><div class=countdown-label>Day</div></div><div class=col-xs-3><div class=countdown-value>13</div><div class=countdown-label>Hours</div></div><div class=col-xs-3><div class=countdown-value>24</div><div class=countdown-label>Minutes</div></div><div class=col-xs-3><div class=countdown-value>10</div><div class=countdown-label>Seconds</div></div></div></div>"
  );


  $templateCache.put('extended-field-render/extended-field-render.html',
    "<div class=\"extended-field-render form-group\"><label ng-if=\"field.type != 'group'\">{{field.title}}</label><div field-transclude></div></div>"
  );


  $templateCache.put('extended-field-render/field-types/multiple-value.html',
    "<div ng-switch=field.type><div class=content-select ng-switch-when=reference><div class=\"content-list list-group\"><div class=\"list-group-item clearfix\" ng-repeat=\"item in model[field.key]\"><a ui-sref=viewContent({id:item._id})><div class=pull-left><img ng-if=\"item._type == 'image'\" ng-src=\"{{$root.getThumbnailUrl(item._id)}}\"> <i ng-if=\"item._type != 'image'\" class=\"fa fa-{{item._type}}\"></i> <i ng-if=\"item.definition == 'song'\" class=\"fa fa-music\" style=padding-right:10px></i> <span>{{item.title}}</span></div></a><div class=\"actions pull-right btn-group\"><a class=\"btn btn-tiny btn-xs\" ng-if=\"item.assetType == 'upload'\" target=_blank ng-href={{$root.getDownloadUrl(item._id)}}><i class=\"fa fa-download\"></i></a> <a class=\"btn btn-tiny btn-xs\" ng-if=canEdit(item) ng-click=editInModal(item)><i class=\"fa fa-edit\"></i></a></div></div></div></div><div ng-switch-default><ul><li ng-repeat=\"value in model[field.key]\">{{value}}</li></ul></div></div>"
  );


  $templateCache.put('extended-field-render/field-types/value.html',
    "<div ng-switch=field.type><div class=content-select ng-switch-when=reference><div class=\"content-list list-group\"><div class=\"list-group-item clearfix\" ng-init=\"item = model[field.key]\"><a ui-sref=viewContent({id:item._id})><div class=pull-left><img ng-if=\"item._type == 'image'\" ng-src=\"{{$root.getThumbnailUrl(item._id)}}\"> <i ng-if=\"item._type != 'image'\" class=\"fa fa-{{item._type}}\"></i> <span>{{item.title}}</span></div></a><div class=\"actions pull-right btn-group\"><a class=\"btn btn-tiny btn-xs\" ng-if=\"item.assetType == 'upload'\" target=_blank ng-href={{$root.getDownloadUrl(item._id)}}><i class=\"fa fa-download\"></i></a></div></div></div></div><div ng-switch-when=date>{{model[field.key] | formatDate:'j M Y'}}</div><div ng-switch-when=image><img ng-src=\"{{$root.asset.imageUrl(item._id)}}\"></div><div ng-switch-default><div ng-bind-html=\"model[field.key] | trusted\"></div></div></div>"
  );


  $templateCache.put('fluro-interaction-form/button-select/fluro-button-select.html',
    "<div id={{options.id}} class=\"button-select {{to.definition.directive}}-buttons\" ng-model=model[options.key]><a ng-repeat=\"(key, option) in to.options\" ng-class={active:contains(option.value)} class=\"btn btn-default\" id=\"{{id + '_'+ $index}}\" ng-click=toggle(option.value)><span>{{option.name}}</span><i class=\"fa fa-check\"></i></a></div>"
  );


  $templateCache.put('fluro-interaction-form/custom.html',
    "<div ng-model=model[options.key] compile-html=to.definition.template></div>"
  );


  $templateCache.put('fluro-interaction-form/date-select/fluro-date-select.html',
    "<div ng-controller=FluroDateSelectController><div class=input-group><input class=form-control datepicker-popup={{format}} ng-model=model[options.key] is-open=opened min-date=to.minDate max-date=to.maxDate datepicker-options=dateOptions date-disabled=\"disabled(date, mode)\" ng-required=to.required close-text=\"Close\"> <span class=input-group-btn><button type=button class=\"btn btn-default\" ng-click=open($event)><i class=\"fa fa-calendar\"></i></button></span></div></div>"
  );


  $templateCache.put('fluro-interaction-form/dob-select/fluro-dob-select.html',
    "<div class=fluro-interaction-dob-select><dob-select ng-model=model[options.key] hide-age=to.params.hideAge hide-dates=to.params.hideDates></dob-select></div>"
  );


  $templateCache.put('fluro-interaction-form/embedded/fluro-embedded.html',
    "<div class=fluro-embedded-form><div class=form-multi-group ng-if=\"to.definition.maximum != 1\"><div class=\"panel panel-default\" ng-init=\"fields = copyFields(); dataFields = copyDataFields(); \" ng-repeat=\"entry in model[options.key] track by $index\"><div class=\"panel-heading clearfix\"><a ng-if=canRemove() class=\"btn btn-danger btn-sm pull-right\" ng-click=\"model[options.key].splice($index, 1)\"><span>Remove {{to.label}}</span><i class=\"fa fa-times\"></i></a><h5>{{to.label}} {{$index + 1}}</h5></div><div class=panel-body><formly-form model=entry fields=fields></formly-form><formly-form model=entry.data fields=dataFields></formly-form></div></div><a class=\"btn btn-primary btn-sm\" ng-if=canAdd() ng-click=addAnother()><span>Add <span ng-if=model[options.key].length>another</span> {{to.label}}</span><i class=\"fa fa-plus\"></i></a></div><div ng-if=\"to.definition.maximum == 1 && options.key\"><formly-form model=model[options.key] fields=options.data.fields></formly-form><formly-form model=model[options.key].data fields=options.data.dataFields></formly-form></div></div>"
  );


  $templateCache.put('fluro-interaction-form/field-errors.html',
    "<div class=field-errors ng-if=\"fc.$touched && fc.$invalid\"><div ng-show=fc.$error.required class=\"alert alert-danger\" role=alert><span class=\"fa fa-exclamation\" aria-hidden=true></span> <span class=sr-only>Error:</span> {{to.label}} is required.</div><div ng-show=fc.$error.validInput class=\"alert alert-danger\" role=alert><span class=\"fa fa-exclamation\" aria-hidden=true></span> <span class=sr-only>Error:</span> <span ng-if=!to.errorMessage.length>'{{fc.$viewValue}}' is not a valid value</span> <span ng-if=to.errorMessage.length>{{to.errorMessage}}</span></div><div ng-show=fc.$error.email class=\"alert alert-danger\" role=alert><span class=\"fa fa-exclamation\" aria-hidden=true></span> <span class=sr-only>Error:</span> <span>'{{fc.$viewValue}}' is not a valid email address</span></div></div>"
  );


  $templateCache.put('fluro-interaction-form/fluro-interaction-input.html',
    "<div class=\"fluro-input form-group\" scroll-active ng-class=\"{'fluro-valid':isValid(), 'fluro-dirty':isDirty, 'fluro-invalid':!isValid()}\"><label><i class=\"fa fa-check\" ng-if=isValid()></i><i class=\"fa fa-exclamation\" ng-if=!isValid()></i><span>{{field.title}}</span></label><div class=\"error-message help-block\"><span ng-if=field.errorMessage>{{field.errorMessage}}</span> <span ng-if=!field.errorMessage>Please provide valid input for this field</span></div><span class=help-block ng-if=\"field.description && field.type != 'boolean'\">{{field.description}}</span><div class=fluro-input-wrapper></div></div>"
  );


  $templateCache.put('fluro-interaction-form/fluro-terms.html',
    "<div class=terms-checkbox><div class=checkbox><label><input type=checkbox ng-model=\"model[options.key]\"> {{to.definition.params.storeData}}</label></div></div>"
  );


  $templateCache.put('fluro-interaction-form/fluro-web-form.html',
    "<div class=fluro-interaction-form><div ng-if=!correctPermissions class=form-permission-warning><div class=\"alert alert-warning small\"><i class=\"fa fa-warning\"></i> <span>You do not have permission to post {{model.plural}}</span></div></div><div ng-if=\"promisesResolved && correctPermissions\"><div ng-if=debugMode><div class=\"btn-group btn-group-justified\"><a ng-click=\"vm.state = 'ready'\" class=\"btn btn-default\">State to ready</a> <a ng-click=\"vm.state = 'complete'\" class=\"btn btn-default\">State to complete</a> <a ng-click=reset() class=\"btn btn-default\">Reset</a></div><hr></div><div ng-show=\"vm.state != 'complete'\"><form novalidate ng-submit=vm.onSubmit()><formly-form model=vm.model fields=vm.modelFields form=vm.modelForm options=vm.options><div ng-if=model.data.recaptcha><div recaptcha-render></div></div><div class=\"form-error-summary form-client-error alert alert-warning\" ng-if=\"vm.modelForm.$invalid && !vm.modelForm.$pristine\"><div class=form-error-summary-item ng-repeat=\"field in errorList\" ng-if=field.formControl.$invalid><i class=\"fa fa-exclamation\"></i> <span ng-if=field.templateOptions.definition.errorMessage.length>{{field.templateOptions.definition.errorMessage}}</span> <span ng-if=!field.templateOptions.definition.errorMessage.length>{{field.templateOptions.label}} has not been provided.</span></div></div><div ng-switch=vm.state><div ng-switch-when=sending><a class=\"btn btn-primary\" ng-disabled=true><span>Processing</span> <i class=\"fa fa-spinner fa-spin\"></i></a></div><div ng-switch-when=error><div class=\"form-error-summary form-server-error alert alert-danger\" ng-if=processErrorMessages.length><div class=form-error-summary-item ng-repeat=\"error in processErrorMessages track by $index\"><i class=\"fa fa-exclamation\"></i> <span>Error processing your submission: {{error}}</span></div></div><button type=submit class=\"btn btn-primary\" ng-disabled=!readyToSubmit><span>Try Again</span> <i class=\"fa fa-angle-right\"></i></button></div><div ng-switch-default><button type=submit class=\"btn btn-primary\" ng-disabled=!readyToSubmit><span>{{submitLabel}}</span> <i class=\"fa fa-angle-right\"></i></button></div></div></formly-form></form></div><div ng-show=\"vm.state == 'complete'\"><div compile-html=transcludedContent></div></div></div></div>"
  );


  $templateCache.put('fluro-interaction-form/nested/fluro-nested.html',
    "<div><div class=form-multi-group ng-if=\"to.definition.maximum != 1\"><div class=\"panel panel-default\" ng-init=\"fields = copyFields()\" ng-repeat=\"entry in model[options.key] track by $index\"><div class=\"panel-heading clearfix\"><a ng-if=canRemove() class=\"btn btn-danger btn-sm pull-right\" ng-click=\"model[options.key].splice($index, 1)\"><span>Remove {{to.label}}</span><i class=\"fa fa-times\"></i></a><h5>{{to.label}} {{$index + 1}}</h5></div><div class=panel-body><formly-form model=entry fields=fields></formly-form></div></div><a class=\"btn btn-primary btn-sm\" ng-if=canAdd() ng-click=addAnother()><span>Add <span ng-if=model[options.key].length>another</span> {{to.label}}</span><i class=\"fa fa-plus\"></i></a></div><div ng-if=\"to.definition.maximum == 1 && options.key\"><formly-form model=model[options.key] fields=options.data.fields></formly-form></div></div>"
  );


  $templateCache.put('fluro-interaction-form/order-select/fluro-order-select.html',
    "<div id={{options.id}} class=fluro-order-select><div ng-if=selection.values.length><p class=help-block>Drag to reorder your choices</p></div><div class=list-group as-sortable=dragControlListeners formly-skip-ng-model-attrs-manipulator ng-model=selection.values><div class=\"list-group-item clearfix\" as-sortable-item ng-repeat=\"item in selection.values\"><div class=pull-left as-sortable-item-handle><i class=\"fa fa-arrows order-select-handle\"></i> <span class=\"order-number text-muted\">{{$index+1}}</span> <span>{{item}}</span></div><div class=\"pull-right order-select-remove\" ng-click=deselect(item)><i class=\"fa fa-times\"></i></div></div></div><div ng-if=canAddMore()><p class=help-block>Choose by selecting options below</p><select class=form-control ng-model=selectBox.item ng-change=selectUpdate()><option ng-repeat=\"(key, option) in to.options | orderBy:'value'\" ng-if=!contains(option.value) value={{option.value}}>{{option.value}}</option></select></div></div>"
  );


  $templateCache.put('fluro-interaction-form/payment/payment-method.html',
    "<hr><div class=payment-method-select><div ng-if=!settings.showOptions><h3 class=clearfix>{{selected.method.title}} <em class=\"pull-right small\" ng-click=\"settings.showOptions = !settings.showOptions\">Other payment options <i class=\"fa fa-angle-right\"></i></em></h3></div><div ng-if=settings.showOptions><h3 class=clearfix>Select payment method <em ng-click=\"settings.showOptions = false\" class=\"pull-right small\">Back <i class=\"fa fa-angle-up\"></i></em></h3><div class=\"payment-method-list list-group\"><div class=\"payment-method-list-item list-group-item\" ng-class=\"{active:method == selected.method}\" ng-click=selectMethod(method) ng-repeat=\"method in methodOptions\"><h5 class=title>{{method.title}}</h5></div></div></div><div ng-if=!settings.showOptions><div ng-if=\"selected.method.key == 'card'\"><formly-form model=model fields=options.data.fields></formly-form></div><div ng-if=\"selected.method == method && selected.method.description.length\" ng-repeat=\"method in methodOptions\"><div compile-html=method.description></div></div></div></div><hr>"
  );


  $templateCache.put('fluro-interaction-form/payment/payment-summary.html',
    "<hr><div class=payment-summary><h3>Payment details</h3><div class=form-group><div ng-if=modifications.length class=payment-running-total><div class=\"row payment-base-row\"><div class=col-xs-6><strong>Base Price</strong></div><div class=\"col-xs-3 col-xs-offset-3\">{{paymentDetails.amount / 100 | currency}}</div></div><div class=\"row text-muted\" ng-repeat=\"mod in modifications\"><div class=col-xs-6><em>{{mod.title}}</em></div><div class=\"col-xs-3 text-right\"><em>{{mod.description}}</em></div><div class=col-xs-3><em class=text-muted>{{mod.total / 100 | currency}}</em></div></div><div class=\"row payment-total-row\"><div class=col-xs-6><h4>Total</h4></div><div class=\"col-xs-3 col-xs-offset-3\"><h4>{{calculatedTotal /100 |currency}} <span class=\"text-uppercase text-muted\">{{paymentDetails.currency}}</span></h4></div></div></div><div class=payment-amount ng-if=!modifications.length>{{calculatedTotal /100 |currency}} <span class=text-uppercase>({{paymentDetails.currency}})</span></div></div></div>"
  );


  $templateCache.put('fluro-interaction-form/search-select/fluro-search-select-item.html',
    "<a class=clearfix><i class=\"fa fa-{{match.model._type}}\"></i> <span ng-bind-html=\"match.label | trusted | typeaheadHighlight:query\"></span> <span ng-if=\"match.model._type == 'event' || match.model._type == 'plan'\" class=\"small text-muted\">// {{match.model.startDate | formatDate:'jS F Y - g:ia'}}</span></a>"
  );


  $templateCache.put('fluro-interaction-form/search-select/fluro-search-select-value.html',
    "<a class=clearfix><span ng-bind-html=\"match.label | trusted | typeaheadHighlight:query\"></span></a>"
  );


  $templateCache.put('fluro-interaction-form/search-select/fluro-search-select.html',
    "<div class=fluro-search-select><div ng-if=\"to.definition.type == 'reference'\"><div class=list-group ng-if=\"multiple && selection.values.length\"><div class=list-group-item ng-repeat=\"item in selection.values\"><i class=\"fa fa-times pull-right\" ng-click=deselect(item)></i> {{item.title}}</div></div><div class=list-group ng-if=\"!multiple && selection.value\"><div class=\"list-group-item clearfix\"><i class=\"fa fa-times pull-right\" ng-click=deselect(selection.value)></i> {{selection.value.title}}</div></div><div ng-if=canAddMore()><div class=input-group><input class=form-control formly-skip-ng-model-attrs-manipulator ng-model=proposed.value typeahead-template-url=fluro-interaction-form/search-select/fluro-search-select-item.html typeahead-on-select=select($item) placeholder=Search typeahead=\"item.title for item in retrieveReferenceOptions($viewValue)\" typeahead-loading=\"search.loading\"><div class=input-group-addon ng-if=!search.loading ng-click=\"search.terms = ''\"><i class=fa ng-class=\"{'fa-search':!search.terms.length, 'fa-times':search.terms.length}\"></i></div><div class=input-group-addon ng-if=search.loading><i class=\"fa fa-spin fa-spinner\"></i></div></div></div></div><div ng-if=\"to.definition.type != 'reference'\"><div class=list-group ng-if=\"multiple && selection.values.length\"><div class=list-group-item ng-repeat=\"value in selection.values\"><i class=\"fa fa-times pull-right\" ng-click=deselect(value)></i> {{getValueLabel(value)}}</div></div><div class=list-group ng-if=\"!multiple && selection.value\"><div class=\"list-group-item clearfix\"><i class=\"fa fa-times pull-right\" ng-click=deselect(selection.value)></i> {{getValueLabel(selection.value)}}</div></div><div ng-if=canAddMore()><div class=input-group><input class=form-control formly-skip-ng-model-attrs-manipulator ng-model=proposed.value typeahead-template-url=fluro-interaction-form/search-select/fluro-search-select-value.html typeahead-on-select=select($item.value) placeholder=Search typeahead=\"item.name for item in retrieveValueOptions($viewValue)\" typeahead-loading=\"search.loading\"><div class=input-group-addon ng-if=!search.loading ng-click=\"search.terms = ''\"><i class=fa ng-class=\"{'fa-search':!search.terms.length, 'fa-times':search.terms.length}\"></i></div><div class=input-group-addon ng-if=search.loading><i class=\"fa fa-spin fa-spinner\"></i></div></div></div></div></div>"
  );


  $templateCache.put('fluro-interaction-form/value/value.html',
    "<div class=fluro-interaction-value style=display:none><pre>{{model[options.key] | json}}</pre></div>"
  );


  $templateCache.put('nametag-printer/nametag-printer.html',
    "<iframe id=print-preview src=/print.html class=print-preview-iframe></iframe>"
  );


  $templateCache.put('nametag/nametag.html',
    "<div class=nametag><div class=nametag-inner><h1 class=nametag-name>{{getDisplayName()}}</h1><h3 class=nametag-event>{{model.event.startDate | formatDate:'g:ia'}} {{model.event.title}}</h3><h3 class=nametag-groups>{{groupNames}}</h3></div></div>"
  );


  $templateCache.put('routes/content/view.html',
    "<div class=\"bg-white border-bottom\"><div class=wrapper-xs><div class=container-fluid><div class=text-wrap><h1><div class=pull-right><i class=\"fa fa-circle\" ng-repeat=\"realm in item.realms\" style=\"margin-left:-30px; color: {{realm.bgColor}}\"></i></div>{{item.title}}</h1><h4 class=text-muted ng-show=definition.title.length>{{definition.title}}</h4><h4 class=text-muted ng-show=!definition.title.length>{{item._type}}</h4><h4 ng-show=\"item.definition == 'song'\"><span class=text-muted>By {{item.data.artist}}</span></h4></div></div></div></div><div ng-switch=item.definition><div ng-switch-when=song><accordion><accordion-title>Song Details</accordion-title><accordion-body><div class=row><div class=\"form-group col-xs-6 col-sm-3\" ng-show=item.data.mm><label>Metronome</label><p>{{item.data.mm}} bpm</p></div><div class=\"form-group col-xs-6 col-sm-3\" ng-show=item.data.key><label>Keys</label><p>{{item.data.key}}</p></div><div class=\"form-group col-xs-12 col-sm-6\" ng-show=item.data.standardStructure><label>Structure</label><p>{{item.data.standardStructure}}</p></div></div><div class=form-group ng-show=item.data.firstLine.length><label>First Line</label><p>{{item.data.firstline}}</p></div></accordion-body></accordion><accordion ng-show=item.data.sheetMusic.length><accordion-title>Sheet Music</accordion-title><accordion-body><div class=list-group><a class=list-group-item ng-repeat=\"file in item.data.sheetMusic\" ng-href=\"{{$root.asset.downloadUrl(file._id, {extension:file.extension})}}\" target=_blank><i class=\"fa fa-download pull-right\"></i> {{file.title}}</a></div></accordion-body></accordion><accordion ng-show=item.data.videos.length><accordion-title>Videos</accordion-title><accordion-body><div ng-repeat=\"video in item.data.videos\"><fluro-video ng-model=video></fluro-video></div></accordion-body></accordion><accordion ng-show=item.data.lyrics.length><accordion-title>Lyrics</accordion-title><accordion-body><div class=list-group-item ng-repeat=\"section in item.data.lyrics\"><h5>{{section.title}}</h5><div compile-html=section.words style=white-space:pre></div></div></accordion-body></accordion></div><div ng-switch-default><div class=wrapper-sm><div class=container-fluid><div class=text-wrap><div view-extended-fields item=item definition=definition></div></div></div></div></div></div>"
  );


  $templateCache.put('routes/home/home.html',
    "<div ng-if=$root.user._id class=printer-panel><div class=printer-header><div class=\"top-bar bg-white\"><div class=row><div class=col-sm-4>{{$root.user.account.title}}</div><div class=\"col-sm-4 text-center\"></div><div class=\"col-sm-4 text-right\"><a ng-click=$root.logout()>Sign out</a></div></div></div></div><div class=printer-content><div class=\"row row-flex\"><div class=\"col-sm-6 bg-white center-flex\"><div class=container-fluid><div class=text-wrap><h1 class=printer-code>{{localStorage.printerID}}</h1><div class=connection-status ng-class=\"{'online':$root.connection.online, 'offline':!$root.connection.online}\"><i class=\"fa fa-fw\" ng-class=\"{'fa-check':$root.connection.online, 'fa-exclamation':!$root.connection.online}\"></i> <span ng-if=$root.connection.online>Internet Connected</span> <span ng-if=!$root.connection.online>Internet Disconnected</span></div><div class=\"panel panel-default\" ng-if=printService.printer><div class=panel-body><h5 style=\"margin: 5px 0\">{{printService.printer.printerName}} <span class=\"small text-muted\" ng-if=\"printService.printer.printerName != printService.printer.deviceName\">({{printService.printer.deviceName}})</span></h5><div class=\"small brand-success\" ng-if=\"printService.printer.printerOptions['printer-state-reasons'] == 'none'\"><i class=\"fa fa-check brand-success\"></i> Ready to print</div><div class=brand-{{printService.getStatus(printService.printer)}} ng-if=\"printService.printer.printerOptions['printer-state-reasons'] != 'none'\"><i class=\"fa fa-warning brand-warning\"></i>{{printService.printer.printerOptions['printer-state-reasons']}}</div><a ng-click=printService.deselectPrinter() ng-if=printService.printer class=\"text-muted small\">Change printer</a></div></div><div ng-if=!printService.availablePrinters.length>No printers are connected to this device</div><div ng-if=\"printService.availablePrinters.length > 1\"><label ng-if=!printService.printer>Change printer</label><div class=list-group ng-if=!printService.printer><div class=\"list-group-item small\" ng-repeat=\"printer in printService.availablePrinters\" ng-click=printService.select(printer) ng-class=\"{'active':printService.printer.deviceName == printer.deviceName}\"><div>{{printer.printerName}}</div><div class=\"small brand-success\" ng-if=\"printer.printerOptions['printer-state-reasons'] == 'none'\"><i class=\"fa fa-check brand-success\"></i> Ready to print</div><div class=brand-{{printService.getStatus(printService.printer)}} ng-if=\"printer.printerOptions['printer-state-reasons'] != 'none'\"><i class=\"fa fa-warning brand-warning\"></i>{{printer.printerOptions['printer-state-reasons']}}</div></div></div></div><div style=display:none><print-preview></print-preview></div></div></div></div><div class=\"col-sm-6 border-left\" ng-switch=settings.uiState><div class=btn-tabs><a class=btn-tab ng-class=\"{active:settings.uiState == 'queue'}\" ng-click=\"settings.uiState = 'queue'\">Print Queue <span class=text-muted>({{printService.queue.length}})</span></a> <a class=btn-tab ng-class=\"{active:settings.uiState == 'history'}\" ng-click=\"settings.uiState = 'history'\">Print History <span class=text-muted>({{printService.history.length}})</span></a> <a class=btn-tab ng-class=\"{active:settings.uiState == 'config'}\" ng-click=\"settings.uiState = 'config'\"><i class=\"fa fa-cog\"></i></a></div><div class=scroll-column ng-switch-when=config><div class=wrapper-sm><div class=container-fluid><div class=form-group><label>Printer ID</label><p class=help-block>Any checkins made tagged with this printer id will be automatically printed</p><input class=\"text-uppercase form-control\" ng-model=localStorage.printerID placeholder=\"Unique Printer ID\"></div><div class=form-group><label><input type=checkbox ng-model=\"printService.previewBeforePrinting\"> Show print preview dialog before printing</label><p class=help-block>Show the print preview dialog allowing you to select paper sizes and adjust printer settings before printing. (this should be turned off during a live checkin session)</p></div><div class=form-group><a class=\"btn btn-default\" ng-click=addToPrintQueue(testCheckin)>Print a test label</a></div></div></div></div><div class=print-queue ng-switch-default><div class=print-queue-item ng-repeat=\"item in printService.queue track by item.id\"><div class=row><div class=\"col-xs-2 text-center\">{{$index+1}}</div><div class=col-xs-6>{{item.title}}</div><div class=col-xs-2><span class=\"text-muted small\">{{item.created | formatDate:'g:ia'}}</span></div></div></div></div><div class=print-history ng-switch-when=history><div class=print-queue-item ng-repeat=\"item in printService.history  | reverse track by item.id\"><div class=row><div class=\"col-xs-2 text-center\">{{printService.history.length - $index}}</div><div class=col-xs-6>{{item.title}}</div><div class=col-xs-2><span class=\"text-muted small\">{{item.created | formatDate:'g:ia'}}</span></div><div class=col-xs-2><a class=\"btn btn-xs btn-link\" ng-click=printService.reprint(item)><i class=\"fa fa-undo\"></i></a></div></div></div></div></div></div></div></div><div class=curtain ng-if=!$root.user._id><div class=container><div class=text-wrap ng-controller=UserLoginController><div style=\"max-width: 320px; margin:auto\"><h3>Sign In</h3><form ng-submit=login({application:!$root.staging})><div class=form-group><label>Email Address</label><input ng-model=credentials.username class=form-control placeholder=\"john@appleseed.com\"></div><div class=form-group><label>Password</label><input ng-model=credentials.password type=password class=form-control placeholder=\"Password\"></div><div class=row><div class=\"col-xs-12 col-sm-5 stacked-xs\"><button class=\"btn btn-primary btn-block\"><span>Sign in</span> <i class=\"fa fa-angle-right\"></i></button></div></div></form></div></div></div></div>"
  );

}]);
